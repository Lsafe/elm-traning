module Main exposing (..)

import Html
import String

--An prefix function named `~=` that takes two Strings and returns True when the first letter of each string is the same
(~=) a b = 
    String.left 1 a == String.left 1 b

main : Html.Html msg
main = 
    (~=)"Manu" "Manu"
        |>  toString 
        |>  Html.text

