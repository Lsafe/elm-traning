module Main exposing (..)

import Html
import String 

wordCount = 
    String.words >> List.length

main : Html.Html msg
main = 
    "Why do you Elm"
    |>wordCount
    |> toString 
    |> Html.text