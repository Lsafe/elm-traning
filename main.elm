module Main exposing (..)
import Html

add a b = 
    a + b

result = 
    --add 1 2
    
    --curried functions
    --add 1 2 |> add 3

    --anonymous functions
      add 2 2|> \a -> a % 2 == 0



main : Html.Html msg
main = 
    Html.text (toString result)

