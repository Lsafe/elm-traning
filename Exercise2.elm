module Main exposing (..)

import Html
import String

--Function to capitalize a name if it has 10 character

capitalize maxLength name = 
    if String.length name > maxLength then
        String.toUpper name
    else
        name

main : Html.Html msg
main = 
    let
        name =
            "Manu Dauhel"

        length =
            String.length name
    in
        (capitalize 10 name)
        
            |> Html.text